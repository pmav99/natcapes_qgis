# About

This repository contains NATCAPES, an algorithm provider for QGIS' Processing
Toolbox and an interface to the GRASS GIS plugin
[`r.estimap.recreation`](https://grass.osgeo.org/grass74/manuals/addons/r.estimap.recreation.html).

# Installation

1. Install [GRASS GIS (any) version 7.x ](https://grass.osgeo.org/download/)

2. Install the GRASS GIS add-on [`r.estimap.recreation`](https://grass.osgeo.org/grass74/manuals/addons/r.estimap.recreation.html) from inside a GRASS GIS
session via one of the following options using the [`g.extension`](https://grass.osgeo.org/grass76/manuals/g.extension.html) module:

    1. pointing to the official GRASS GIS Add-on SVN repository
        ```
        g.extension r.estimap.recreation
        ```
    2. pointing to the gitlab repository of the add-on
        ```
        g.extension r.estimap.recreation url=https://gitlab.com/natcapes/r.estimap.recreation/-/archive/master/r.estimap.recreation-master.zip
        ```
    3. downloading the zip file manually from the gitlab repository, or via the command line
        ```
        wget https://gitlab.com/natcapes/r.estimap.recreation/-/archive/master/r.estimap.recreation-master.zip
        ```
        and then feed the `g.extension`'s `url=` option to the ZIP file
        ```
        g.extension r.estimap.recreation url=r.estimap.recreation-master.zip
        ```
        
    2. if already installed, from inside QGIS LTR (version >= 3.4.5), the GRASS
    GIS add-on `r.estimap.recreation` can be installed via the QGIS "GRASS Plugin" (see [GRASS GIS Integration](https://docs.qgis.org/2.18/en/docs/user_manual/grass_integration/grass_integration.html)):
        1. [open a GRASS Mapset](https://docs.qgis.org/2.18/en/docs/user_manual/grass_integration/grass_integration.html#opening-grass-mapset)
        2. [open the GRASS Shell](https://docs.qgis.org/2.18/en/docs/user_manual/grass_integration/grass_integration.html#working-with-grass-modules)
        3. install the add-on via
            ```
            g.extension r.estimap.recreation
            ```

3. Install [QGIS LTR](https://qgis.org/en/site/forusers/download.html) and
    
4. Install the NATCAPES algorithm provider for QGIS' Processing Toolbox
    1. download the ['natcapes_qgis' QGIS plugin as a .zip file](https://gitlab.com/natcapes/natcapes_qgis/-/archive/master/natcapes_qgis-master.zip) from "your"
    NATCAPES gitlab repository [4]
    2. [install the ZIP file using QGIS' dedicated interface inside the Plugin Manager](https://docs.qgis.org/testing/en/docs/user_manual/plugins/plugins.html#the-install-from-zip-tab)

## Notes

The installation tested with the latest QGIS LTR version under the following operating systems:

* Linux 64-bit
    * Ubuntu 18.04
    * Funtoo-Linux

* Windows
    * Windows 7 (inside a virtual machine)
    * Windows 8.1
    * Windows 10 Home